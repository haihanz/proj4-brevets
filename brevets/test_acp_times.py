from acp_times import open_time
from acp_times import close_time

def test_open_time():
    assert open_time(0, 1000,'2017-01-01T00:00:00+00:00') == 'Sun 1/1 0:00'
    assert open_time(150, 1000,'2017-01-01T00:00:00+00:00') == 'Sun 1/1 4:24'
    assert open_time(200, 1000,'2017-01-01T00:00:00+00:00') == 'Sun 1/1 5:52'
    assert open_time(400, 1000,'2017-01-01T00:00:00+00:00') == 'Sun 1/1 12:07'
    assert open_time(600, 1000,'2017-01-01T00:00:00+00:00') == 'Sun 1/1 18:47'
    assert open_time(1000, 1000,'2017-01-01T00:00:00+00:00') == 'Mon 1/2 9:05'

def test_close_time():
    assert close_time(0, 1000,'2017-01-01T00:00:00+00:00') == 'Sun 1/1 1:00'
    assert close_time(150, 1000,'2017-01-01T00:00:00+00:00') == 'Sun 1/1 10:00'
    assert close_time(200, 1000,'2017-01-01T00:00:00+00:00') == 'Sun 1/1 13:20'
    assert close_time(400, 1000,'2017-01-01T00:00:00+00:00') == 'Mon 1/2 2:40'
    assert close_time(600, 1000,'2017-01-01T00:00:00+00:00') == 'Mon 1/2 16:00'
    assert close_time(1000, 1000,'2017-01-01T00:00:00+00:00') == 'Wed 1/4 3:00'
def test_bound():
    assert open_time(200, 200,'2017-01-01T00:00:00+00:00') == 'Sun 1/1 5:52'
    assert open_time(205, 200,'2017-01-01T00:00:00+00:00') == 'Sun 1/1 5:52'
    assert open_time(205, 400,'2017-01-01T00:00:00+00:00') == 'Sun 1/1 6:02'
    assert close_time(200, 200,'2017-01-01T00:00:00+00:00') == 'Sun 1/1 13:20'
    assert close_time(205, 200,'2017-01-01T00:00:00+00:00') == 'Sun 1/1 13:20'
    assert close_time(205, 400,'2017-01-01T00:00:00+00:00') == 'Sun 1/1 13:40'