"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    begin = arrow.get(brevet_start_time)
    if control_dist_km == 0:
        time = begin
    if control_dist_km <= 200:
        H = control_dist_km/34
        time = begin.shift(hours=H)
    if 200 < control_dist_km <= 400:
        H = 200/34 + (control_dist_km - 200)/32
        time = begin.shift(hours=H)
    if 400 < control_dist_km <= 600:
        H = 200/34 + 200/32 + (control_dist_km - 400)/30
        time = begin.shift(hours=H)
    if 600 < control_dist_km <= 1000:
        H = 200/34 + 200/32 + 200/30 + (control_dist_km - 600)/28
        time = begin.shift(hours=H)
    elif brevet_dist_km == 200:
        if 200 < control_dist_km <= 240:
            H = 200/34
            time = begin.shift(hours=H)
    elif brevet_dist_km == 400:
        if 400 < control_dist_km <= 480:
            H = 400/32
            time = begin.shift(hours=H)
    elif brevet_dist_km == 600:
        if 600 < control_dist_km <= 720:
            H = 600/30
            time = begin.shift(hours=H)
    elif brevet_dist_km == 1000:
        if 1000 < control_dist_km <= 1200:
            H = 1000/28
            time = begin.shift(hours=H)
    
    return time.format("ddd M/D H:mm")

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    begin = arrow.get(brevet_start_time)
    
    if control_dist_km <= 600:
        H = control_dist_km / 15
        time = begin.shift(hours=H)
    if 600 < control_dist_km <= 1000:
        H = 600/15 + (control_dist_km - 600)/11.428
        time = begin.shift(hours=H)
    elif control_dist_km == 0:
        time = begin.shift(hours=1)
    elif brevet_dist_km == 200:
        if 200 < control_dist_km <= 240:
            H = 200/15
            time = begin.shift(hours=H)
    elif brevet_dist_km == 400:
        if 400 < control_dist_km <= 480:
            H = 400/15
            time = begin.shift(hours=H)
    elif brevet_dist_km == 600:
        if 600 < control_dist_km <= 720:
            H = 600/15
            time = begin.shift(hours=H)
    elif brevet_dist_km == 1000:
        if 1000 < control_dist_km <= 1200:
            H = 1000/11.428
            time = begin.shift(hours=H)
    return time.format("ddd M/D H:mm")

print(open_time(200, 200,'2017-01-01T00:00:00+00:00'))
print(open_time(205, 200,'2017-01-01T00:00:00+00:00'))
print(open_time(205, 400,'2017-01-01T00:00:00+00:00'))
print(close_time(200, 200,'2017-01-01T00:00:00+00:00'))
print(close_time(205, 200,'2017-01-01T00:00:00+00:00'))
print(close_time(205, 400,'2017-01-01T00:00:00+00:00'))

